/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-06 18:12:12
 * Last Modified: 2017-12-07 13:12:13
 * Modified By: Gaston Siffert
 */

import React, { Component } from 'react';

import Config from "../config.js"

import "../css/MainPage.css"

function CompetitorItem(props) {
    return (
        <tr className="competitor_item" key={props.id}>
            <td>{props.position}</td>
            <td>{props.name}</td>
        </tr>
    );
}

function CompetitorBoard(props) {
    var array = []
    for (var i = 0; i < props.length; i++) {
        var item = {
            position: i + 1, name: props[i].name, id: props[i].id,
        }
        array.push(item)
    }
    var competitorItems = array.map((item) =>
        CompetitorItem(item)
    )

    return (
        <div className="race_board">
            <h3>Competitors :</h3>
            <table>
                <tbody>
                    <tr>
                        <th>Position</th>
                        <th>Name</th>
                    </tr>
                    
                    {competitorItems}
                </tbody>
            </table>
        </div>
    )
}

class RacePage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: props.match.params.id,
            raceTypeID: props.match.params.race_type_id,
            data: undefined,
            onError: undefined
        };
    }

    componentDidMount() {
        this.getRaceSummary(this.state.raceTypeID, this.state.id)
    }

    // We should rather build a library for this. The best option would be a
    // self generated swagger library.
    getRaceSummary(raceTypeID, id) {
        const url = Config().api_host + "/races/" + raceTypeID + "/" + id
        
        fetch(url)
            .then(response => {
                if (!response.ok) {
                    throw new Error("Bad response");
                }
                return response.json()
            })
            .then(responseJson => {
                this.setState({ data: responseJson })
            })
            .catch(error => {
                this.setState({onError: error})
            })
    }

    renderError() {
        return (
            <div>
                <p>Loading, please wait</p>
            </div>
        )
    }

    renderLoading() {
        return (
            <div>
                <p>Loading, please wait</p>
            </div>
        )
    }

    render() {
        if (this.state.error !== undefined) {
            return this.renderError()
        }
        if (this.state.data === undefined) {
            return this.renderLoading()
        }

        return (
            <div>
                <h2>Race: {this.state.data.race_type.name}</h2>
                
                {CompetitorBoard(this.state.data.competitors)}
            </div>
        );
    }
}

export default RacePage