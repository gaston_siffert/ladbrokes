/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-06 17:12:50
 * Last Modified: 2017-12-07 12:12:53
 * Modified By: Gaston Siffert
 */

import React, { Component } from 'react';
import Websocket from 'react-websocket';

import Config from "../config.js"

import "../css/MainPage.css"

function RaceItem(props){

    return (
        <tr className="race_item" key={props.closing_time} onClick={props.onClick}>
            <td>{props.localTime}</td>
            <td>{props.type.name}</td>
            <td>{props.countdown}</td>
        </tr>
    );
}

function RaceBoard(props) {
    var raceItems = props.map((item) =>
        RaceItem(item)
    )

    return (
        <div className="race_board">
            <h2>Next 5 races:</h2>
            <table>
                <tbody>
                    <tr>
                        <th>Closing Time</th>
                        <th>Race Type</th> 
                        <th>Countdown</th>
                    </tr>
                    
                    {raceItems}
                </tbody>
            </table>
        </div>
    )
}

class MainPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            serverTime: "",
            nextRaces: []
        };
    }

    componentDidMount() {
        console.log(process.env.NODE_ENV)
        this.interval = setInterval(this.tick.bind(this), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    tick() {
        if (this.state.nextRaces === undefined) {
            return
        }

        var races = this.generateRacesWithCountdown(this.state.nextRaces)
        this.setState({nextRaces: races})
    }

    timeDiff(a, b) {
        var diff = b - a
        var seconds = diff / 1000
        return seconds.toFixed(0)
    }

    generateRacesWithCountdown(nextRaces) {
        var races = []
        var now = new Date()

        for (var i = 0; i < nextRaces.length; i++) {
            var race = nextRaces[i]

            var closingTime = new Date(race.closing_time)
            var newRace = {
                closing_time: race.closing_time,
                localTime: closingTime.toLocaleTimeString(),
                type: race.type,
                meeting_link: race.meeting_link,
                countdown: this.timeDiff(now, closingTime),
                onClick: this.onClick(i).bind(this)
            }
            races.push(newRace)
        }

        return races
    }

    handleData(data) {
        let result = JSON.parse(data);

        var races = this.generateRacesWithCountdown(result.next_races)
        this.setState({serverTime: result.server_time, nextRaces: races});
    }

    onClick(index) {
        return function() {
            var item = this.state.nextRaces[index]

            var route = "/races/" + item.type.id + "/" + (index + 1)
            this.props.history.push(route);
        }
    }

    render() {
        return (
          <div>
            {RaceBoard(this.state.nextRaces)}
   
            <Websocket url={Config().proxy_host} onMessage={this.handleData.bind(this)}/>
          </div>
        );
    }
}

export default MainPage