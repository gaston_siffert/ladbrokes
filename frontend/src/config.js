/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-07 12:12:51
 * Last Modified: 2017-12-07 13:12:03
 * Modified By: Gaston Siffert
 */

const development = {
    api_host: "http://localhost:8082",
    proxy_host: "ws://localhost:8083"
}

function Config() {
    switch (process.env.NODE_ENV) {
        case "development":
            return development
        default:
            return development
    }
}

export default Config