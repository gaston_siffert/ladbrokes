/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-06 17:12:54
 * Last Modified: 2017-12-07 12:12:34
 * Modified By: Gaston Siffert
 */

import React from 'react';
import {
    BrowserRouter as Router,
    Route,
} from 'react-router-dom'
import './App.css'

import MainPage from './components/MainPage';
import RacePage from './components/RacePage';

const App = () => (
    <Router>
      <div className="root">
        <header>
            <h1>Next5, Ladbrokes</h1>
        </header>

        <main>
           <Route exact path="/" component={MainPage}/>
           <Route path="/races/:race_type_id/:id" component={RacePage} />
        </main>

        <footer>
            Copyright Gaston Siffert
        </footer>
      </div>
    </Router>
  )

export default App