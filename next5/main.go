/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-05 19:12:53
 * Last Modified: 2017-12-06 09:12:06
 * Modified By: Gaston Siffert
 */

package main

import (
	"log"
	"math/rand"
	"time"

	"bitbucket.org/gaston_siffert/ladbrokes/next5/database"
	"bitbucket.org/gaston_siffert/ladbrokes/next5/engine"
	"bitbucket.org/gaston_siffert/ladbrokes/next5/publisher"
)

func onError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	rand.Seed(time.Now().UTC().UnixNano())

	conf, err := getConfiguration()
	onError(err)

	// we setup a random interval of 20 minutes, for testing sake
	db, err := database.NewRandomGenerator(conf.MaxInterval, conf.APIHost)
	onError(err)
	defer db.Close()

	// the application will run on kubernetes, so the internal port
	// doesn't matter, it can be changed later in the kubernetes service
	publisher, err := publisher.NewZMQPublisher("tcp://*:8080")
	onError(err)
	defer publisher.Close()

	engine := engine.NewDefaultEngine(publisher, db, conf.Topic)
	onError(engine.StartStream())
}
