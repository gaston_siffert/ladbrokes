/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-07 17:12:48
 * Last Modified: 2017-12-07 18:12:06
 * Modified By: Gaston Siffert
 */

package publisher

import (
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

type newZMQPublisherTest struct {
	listenOn string
	err      error
}

var newZMQPublisherTests = []newZMQPublisherTest{
	newZMQPublisherTest{
		err: errors.New("invalid argument"),
	},
	newZMQPublisherTest{
		listenOn: "not a protocol",
		err:      errors.New("invalid argument"),
	},
	newZMQPublisherTest{
		listenOn: "tcp://*",
		err:      errors.New("invalid argument"),
	},
	newZMQPublisherTest{
		listenOn: "tcp://*:12345", // Please don't use this port while testing
	},
}

func testNewZMQPublisher(t *testing.T, test newZMQPublisherTest) {
	t.Run(fmt.Sprintf("%v", test), func(t *testing.T) {
		_, err := NewZMQPublisher(test.listenOn)
		if test.err != nil {
			assert.Equal(t, test.err.Error(), err.Error())
		}
	})
}

func TestNewZMQPublisher(t *testing.T) {
	for _, test := range newZMQPublisherTests {
		testNewZMQPublisher(t, test)
	}
}
