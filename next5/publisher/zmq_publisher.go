/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-05 21:12:50
 * Last Modified: 2017-12-06 08:12:44
 * Modified By: Gaston Siffert
 */

package publisher

import (
	zmq "github.com/pebbe/zmq4"
)

// ZMQPublisher implement the publisher interface with ZMQ
type ZMQPublisher struct {
	toClients *zmq.Socket
}

// NewZMQPublisher instantiate a new ZMQPublisher
func NewZMQPublisher(listenOn string) (*ZMQPublisher, error) {
	toClients, err := zmq.NewSocket(zmq.PUB)
	if err != nil {
		return nil, err
	}

	err = toClients.Bind(listenOn)
	return &ZMQPublisher{
		toClients: toClients,
	}, err
}

// Broadcast send a message to every subscribers
func (z *ZMQPublisher) Broadcast(topic string, message []byte) error {
	_, err := z.toClients.SendMessage(topic, message)
	return err
}

// Close the pending resources
func (z *ZMQPublisher) Close() {
	if z.toClients != nil {
		z.toClients.Close()
	}
}
