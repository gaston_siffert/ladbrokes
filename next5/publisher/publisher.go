/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-05 21:12:23
 * Last Modified: 2017-12-06 08:12:16
 * Modified By: Gaston Siffert
 */

package publisher

// Publisher interface for the supported wiring protocol
type Publisher interface {
	// Broadcast send a message to every subscribers
	Broadcast(topic string, message []byte) error
	// Close the pending resources
	Close()
}
