/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-07 18:12:33
 * Last Modified: 2017-12-07 19:12:44
 * Modified By: Gaston Siffert
 */

package engine

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"testing"
	"time"

	"bitbucket.org/gaston_siffert/ladbrokes/next5/database"
	"bitbucket.org/gaston_siffert/ladbrokes/next5/publisher"
	"github.com/stretchr/testify/assert"
)

const (
	alwaysFail = "always fail"
)

type nexTimerTest struct {
	stack    []database.Race
	expected time.Duration
}

// We don't test the empty list, it shouldn't be a problem and I want us to
// crash if it happens. It is a design error.
var nexTimerTests = []nexTimerTest{
	nexTimerTest{
		stack: []database.Race{
			database.Race{},
			database.Race{},
		},
		expected: time.Duration(5 * time.Minute),
	},
	nexTimerTest{
		stack: []database.Race{
			database.Race{},
			database.Race{},
			database.Race{},
		},
		expected: time.Duration(5 * time.Second),
	},
}

func testNextTimer(t *testing.T, test nexTimerTest) {
	e := DefaultEngine{}

	t.Run(fmt.Sprintf("%v", test), func(t *testing.T) {
		now := time.Now()
		test.stack[0].ClosingTime = now.Add(test.expected)

		duration := e.nexTimer(test.stack, now)
		assert.Equal(t, test.expected, duration)
	})
}

func TestNextTimer(t *testing.T) {
	for _, test := range nexTimerTests {
		testNextTimer(t, test)
	}
}

type next5Test struct {
	stack []database.Race
	err   error
}

// We don't test tif the list is smaller than the pool size,
// it shouldn't be a problem and I want us to crash if it happens.
// It is a design error.
var next5Tests = []next5Test{
	next5Test{
		stack: []database.Race{
			database.Race{
				ClosingTime: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC),
				MeetingLink: "http://host.com/1/1",
				Type: database.RaceType{
					ID: 1, Name: "race 1",
				},
			},
			database.Race{
				ClosingTime: time.Date(2000, 1, 1, 0, 0, 1, 0, time.UTC),
				MeetingLink: "http://host.com/1/2",
				Type: database.RaceType{
					ID: 1, Name: "race 2",
				},
			},
			database.Race{
				ClosingTime: time.Date(2000, 1, 1, 0, 0, 5, 0, time.UTC),
				MeetingLink: "http://host.com/1/3",
				Type: database.RaceType{
					ID: 1, Name: "race 3",
				},
			},
			database.Race{
				ClosingTime: time.Date(2000, 1, 1, 0, 0, 8, 0, time.UTC),
				MeetingLink: "http://host.com/1/4",
				Type: database.RaceType{
					ID: 1, Name: "race 4",
				},
			},
			database.Race{
				ClosingTime: time.Date(2000, 1, 1, 0, 1, 0, 0, time.UTC),
				MeetingLink: "http://host.com/1/5",
				Type: database.RaceType{
					ID: 1, Name: "race 5",
				},
			},
			database.Race{
				ClosingTime: time.Date(2000, 1, 1, 0, 1, 2, 0, time.UTC),
				MeetingLink: "http://host.com/1/6",
				Type: database.RaceType{
					ID: 1, Name: "race 6",
				},
			},
		},
	},
}

func testNext5(t *testing.T, test next5Test) {
	e := DefaultEngine{}

	t.Run(fmt.Sprintf("%v", test), func(t *testing.T) {
		now := time.Now()
		bytes, err := e.next5(test.stack)
		assert.Equal(t, test.err, err)

		message := broadcastMessage{}
		err = json.Unmarshal(bytes, &message)
		// Invalid test
		if err != nil {
			log.Fatal(err)
		}

		assert.Equal(t, test.stack[0:5], message.NextRaces)
		assert.True(t, now.Before(message.ServerTime))
	})
}

func TestNext5(t *testing.T) {
	for _, test := range next5Tests {
		testNext5(t, test)
	}
}

type startStreamTest struct {
	db  database.Database
	pub publisher.Publisher
	err error
}

type alwaysFailDB struct{}

func (a alwaysFailDB) NextRaces(pages uint) ([]database.Race, error) {
	return nil, errors.New(alwaysFail)
}
func (a alwaysFailDB) Close() {}

type alwaysSucceedDB struct{}

func (a alwaysSucceedDB) NextRaces(pages uint) ([]database.Race, error) {
	races := make([]database.Race, bufferSize)
	for i := 0; i < bufferSize; i++ {
		races[i] = database.Race{}
	}

	return races, nil
}
func (a alwaysSucceedDB) Close() {}

type emptyDB struct{}

func (e emptyDB) NextRaces(pages uint) ([]database.Race, error) {
	return []database.Race{}, nil
}
func (e emptyDB) Close() {}

type alwaysFailPub struct{}

func (a alwaysFailPub) Broadcast(topic string, message []byte) error {
	return errors.New(alwaysFail)
}
func (a alwaysFailPub) Close() {}

// We only test when it should fail, we should add another function
// for when it works
var startStreamTests = []startStreamTest{
	startStreamTest{
		db:  alwaysFailDB{},
		err: errors.New(alwaysFail),
	},
	startStreamTest{
		db:  emptyDB{},
		err: errors.New(emptyNext),
	},
	startStreamTest{
		db:  alwaysSucceedDB{},
		pub: alwaysFailPub{},
		err: errors.New(alwaysFail),
	},
}

func testStartStream(t *testing.T, test startStreamTest) {
	e := DefaultEngine{db: test.db, pub: test.pub}

	t.Run(fmt.Sprintf("%v", test), func(t *testing.T) {
		err := e.StartStream()
		assert.Equal(t, test.err, err)
	})
}

func TestStartStream(t *testing.T) {
	for _, test := range startStreamTests {
		testStartStream(t, test)
	}
}
