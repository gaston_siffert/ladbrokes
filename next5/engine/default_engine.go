/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-05 19:12:25
 * Last Modified: 2017-12-07 19:12:37
 * Modified By: Gaston Siffert
 */

package engine

import (
	"encoding/json"
	"errors"
	"time"

	"bitbucket.org/gaston_siffert/ladbrokes/next5/database"
	"bitbucket.org/gaston_siffert/ladbrokes/next5/publisher"
)

const (
	pool       = 5
	bufferSize = 4 * pool
	emptyNext  = "there aren't any races scheduled at the moment"
)

// DefaultEngine default implementation of the Engine interface
type DefaultEngine struct {
	pub   publisher.Publisher
	db    database.Database
	topic string
}

// NewDefaultEngine instantiate a new DefaultEngine with properly
// initialised fields
func NewDefaultEngine(pub publisher.Publisher, db database.Database, topic string) *DefaultEngine {

	return &DefaultEngine{
		pub:   pub,
		db:    db,
		topic: topic,
	}
}

// nexTimer return the timeDuration between the next closing time and now
func (d *DefaultEngine) nexTimer(stack []database.Race, now time.Time) time.Duration {
	return stack[0].ClosingTime.Sub(now)
}

func (d *DefaultEngine) next5(stack []database.Race) ([]byte, error) {
	message := broadcastMessage{
		NextRaces:  stack[0:5],
		ServerTime: time.Now(),
	}
	return json.Marshal(message)
}

// StartStream start a loop of streaming event
func (d *DefaultEngine) StartStream() error {
	stack := []database.Race{}
	timer := time.NewTimer(0)

	for {
		// we bufferize the next races, I assume the closing time won't change
		// once scheduled. We could / should reload this list before we
		// empty the current stack.
		nextRaces, err := d.db.NextRaces(bufferSize)
		if err != nil {
			return err
		} else if len(nextRaces) == 0 {
			return errors.New(emptyNext)
		}
		stack = append(stack, nextRaces...)

		// set the timer to broadcast the clients for the end of the next
		// closing time
		timer.Reset(d.nexTimer(stack, time.Now()))
		for len(stack) > pool {
			<-timer.C

			// Pop the head of the slice, removing the outdated race
			stack = stack[1:]
			timer.Reset(d.nexTimer(stack, time.Now()))

			// publish the next five races
			bytes, _ := d.next5(stack)
			if err := d.pub.Broadcast(d.topic, bytes); err != nil {
				return err
			}
		}
	}
}

// Close the pending resources
func (d *DefaultEngine) Close() {}
