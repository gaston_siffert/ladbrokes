/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-05 19:12:25
 * Last Modified: 2017-12-06 08:12:11
 * Modified By: Gaston Siffert
 */

package engine

import (
	"time"

	"bitbucket.org/gaston_siffert/ladbrokes/next5/database"
)

// Engine interface to abstract the core engine of this streamer app
type Engine interface {
	// StartStream start a loop of streaming event
	StartStream() error
	// Close the pending resources
	Close()
}

// broadcastMessage message sent through the wire, it must be versioned
// and well documented for the client'sake.
// TODO move to protocolbuffer
type broadcastMessage struct {
	// ServerTime when the server sent the message, required to have an accurate
	// count down in client side
	ServerTime time.Time `json:"server_time"`
	// NextRaces list of the next five races
	NextRaces []database.Race `json:"next_races"`
}
