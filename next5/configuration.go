/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-06 08:12:32
 * Last Modified: 2017-12-07 17:12:01
 * Modified By: Gaston Siffert
 */

package main

import (
	"fmt"
	"io/ioutil"
	"os"

	yaml "gopkg.in/yaml.v2"
)

const (
	configFile     = "./config.yaml"
	envKey         = "RUN_ENV"
	errEnvNotFound = "the env: %s hasn't been found in the file %s"
)

type configuration struct {
	APIHost     string `yaml:"api_host"`
	MaxInterval int64  `yaml:"random_max_interval"`
	Topic       string `yaml:"topic"`
}

func getConfigFromFile(fileName string, env string) (*configuration, error) {
	// Parse a yaml file
	bytes, err := ioutil.ReadFile(fileName)
	if err != nil {
		return nil, err
	}
	configurations := map[string]configuration{}
	if err := yaml.Unmarshal(bytes, &configurations); err != nil {
		return nil, err
	}

	// retrieve the configuration matching the running env
	conf, ok := configurations[env]
	if !ok {
		return nil, fmt.Errorf(errEnvNotFound, env, configFile)
	}
	return &conf, nil
}

func getConfiguration() (*configuration, error) {
	env := os.Getenv(envKey)
	return getConfigFromFile(configFile, env)
}
