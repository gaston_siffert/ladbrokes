/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-05 20:12:04
 * Last Modified: 2017-12-06 08:12:43
 * Modified By: Gaston Siffert
 */

package database

const (
	pageSize = 5
)

// Database interface to wrap the query in our database
type Database interface {
	// NextRaces retrieve the next N races (pages * pageSize)
	NextRaces(pages uint) ([]Race, error)
	// Close to close the pending resources
	Close()
}
