/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-07 17:12:02
 * Last Modified: 2017-12-07 18:12:16
 * Modified By: Gaston Siffert
 */

package database

import (
	"errors"
	"fmt"
	"regexp"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func isUnique(t *testing.T) func(i interface{}) {
	counter := map[interface{}]bool{}

	return func(item interface{}) {
		_, found := counter[item]
		assert.False(t, found)
		counter[item] = true
	}
}

type newRandomGeneratorTest struct {
	maxInterval     int64
	apiHost         string
	randomGenerator *RandomGenerator
	err             error
}

var newRandomGeneratorTests = []newRandomGeneratorTest{
	newRandomGeneratorTest{
		maxInterval: -1,
		err:         errors.New(invalidInterval),
	},
	newRandomGeneratorTest{
		maxInterval: 0,
		err:         errors.New(invalidInterval),
	},
	newRandomGeneratorTest{
		maxInterval: 10,
		apiHost:     "http://host.com",
		randomGenerator: &RandomGenerator{
			apiHost:     "http://host.com",
			maxInterval: 10,
		},
	},
}

func testNewRandomGeneratorTest(t *testing.T, test newRandomGeneratorTest) {
	t.Run(fmt.Sprintf("%v", test), func(t *testing.T) {
		randomGenerator, err := NewRandomGenerator(test.maxInterval, test.apiHost)
		assert.Equal(t, test.randomGenerator, randomGenerator)
		if test.err != nil {
			assert.Equal(t, test.err.Error(), err.Error())
		}
	})
}

func TestNewRandomGeneratorTest(t *testing.T) {
	for _, test := range newRandomGeneratorTests {
		testNewRandomGeneratorTest(t, test)
	}
}

func TestRandomTime(t *testing.T) {
	unique := isUnique(t)
	generator := RandomGenerator{maxInterval: 10000}

	t.Run(fmt.Sprintf("%v", generator), func(t *testing.T) {
		for i := 0; i < 100-0; i++ {
			time := generator.randomTime()
			assert.NotZero(t, time)
			unique(time)
		}
	})
}

var allowedRaces = map[string]bool{
	"thoroughbred": true,
	"greyhounds":   true,
	"harness":      true,
}

func TestRandomRaceType(t *testing.T) {
	counter := map[string]uint{}
	generator := RandomGenerator{}

	t.Run(fmt.Sprintf("%v", generator), func(t *testing.T) {
		for i := 0; i < 10000; i++ {
			raceType := generator.randomRaceType()
			assert.True(t, allowedRaces[raceType.Name])
			counter[raceType.Name] = counter[raceType.Name] + 1
		}

		// With 10000 of call, the probability of having 2 same count
		// one after one should be low enough
		last := uint(0)
		for _, value := range counter {
			assert.NotEqual(t, last, value)
			last = value
		}
	})
}

type generateMeetingLinkTest struct {
	raceID   uint
	host     string
	expected string
}

var generateMeetingLinkTests = []generateMeetingLinkTest{
	generateMeetingLinkTest{
		raceID:   1,
		host:     "http://host.com",
		expected: "http://host.com/races/1/[1-9][0-9]*",
	},
	generateMeetingLinkTest{
		raceID:   1,
		host:     "http://something.com",
		expected: "http://something.com/races/1/[1-9][0-9]*",
	},
	generateMeetingLinkTest{
		raceID:   3,
		host:     "http://host.com",
		expected: "http://host.com/races/3/[1-9][0-9]*",
	},
}

func testGenerateMeetingLink(t *testing.T, test generateMeetingLinkTest) {
	t.Run(fmt.Sprintf("%v", test), func(t *testing.T) {
		generator := RandomGenerator{apiHost: test.host}
		link := generator.generateMeetingLink(test.raceID)

		matched, err := regexp.MatchString(test.expected, link)
		assert.Nil(t, err)
		assert.True(t, matched)
	})
}

func TestGenerateMeetingLink(t *testing.T) {
	for _, test := range generateMeetingLinkTests {
		testGenerateMeetingLink(t, test)
	}
}

func TestGenerateRate(t *testing.T) {
	unique := isUnique(t)
	generator := RandomGenerator{apiHost: "http://host.com", maxInterval: 1000}

	t.Run(fmt.Sprintf("%v", generator), func(t *testing.T) {
		for i := 0; i < 1000; i++ {
			race := generator.generateRace()
			assert.NotNil(t, race)
			unique(race)
		}
	})
}

type nextRacesTest struct {
	pages  uint
	length int
	err    error
}

var nextRacesTests = []nextRacesTest{
	nextRacesTest{},
	nextRacesTest{
		pages:  5,
		length: 5 * pageSize,
	},
	nextRacesTest{
		pages:  1,
		length: pageSize,
	},
}

func testNextRaces(t *testing.T, test nextRacesTest) {
	t.Run(fmt.Sprintf("%v", test), func(t *testing.T) {
		unique := isUnique(t)
		generator := RandomGenerator{apiHost: "http://host.com", maxInterval: 1000}

		races, err := generator.NextRaces(test.pages)
		assert.Equal(t, test.err, err)
		assert.Equal(t, test.length, len(races))
		before := time.Time{}
		for _, race := range races {
			unique(race)
			// Check if the race are ordered ascendingly
			assert.True(t, before.Before(race.ClosingTime))
			before = race.ClosingTime
		}
	})
}

func TestNextRaces(t *testing.T) {
	for _, test := range nextRacesTests {
		testNextRaces(t, test)
	}
}
