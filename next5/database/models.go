/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-05 20:12:12
 * Last Modified: 2017-12-06 07:12:12
 * Modified By: Gaston Siffert
 */

package database

import "time"

// Race is a model which describe a scheduled race
type Race struct {
	// ClosingTime represent the end of the betting time
	ClosingTime time.Time `json:"closing_time"`
	// Type breeding race in this meeting
	Type RaceType `json:"type"`
	// MeetingLing link to the meeting resource
	MeetingLink string `json:"meeting_link"`
}

// RaceType represent an allowed type of breeding race
type RaceType struct {
	// ID primary key and unique identifier
	ID uint `json:"id"`
	// Name of the breeding race
	Name string `json:"name"`
}
