/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-05 20:12:32
 * Last Modified: 2017-12-07 17:12:34
 * Modified By: Gaston Siffert
 */

package database

import (
	"errors"
	"fmt"
	"math/rand"
	"sort"
	"time"
)

const (
	invalidInterval = "an interval must be > 0"
)

var raceTypes = []RaceType{
	RaceType{ID: 1, Name: "thoroughbred"},
	RaceType{ID: 2, Name: "greyhounds"},
	RaceType{ID: 3, Name: "harness"},
}

// RandomGenerator implement the Database interface with self generating
// resources
type RandomGenerator struct {
	maxInterval int64
	apiHost     string
}

// NewRandomGenerator instantiate a new RandomGenerator with properly
// initialised fields.
// maxInterval in seconds
func NewRandomGenerator(maxInterval int64, apiHost string) (*RandomGenerator, error) {
	if maxInterval <= 0 {
		return nil, errors.New(invalidInterval)
	}

	return &RandomGenerator{
		maxInterval: maxInterval,
		apiHost:     apiHost,
	}, nil
}

func (r *RandomGenerator) randomTime() time.Time {
	randomInterval := rand.Int63n(r.maxInterval)
	interval := time.Second * time.Duration(randomInterval)
	return time.Now().Add(interval)
}

func (r *RandomGenerator) randomRaceType() RaceType {
	i := rand.Intn(len(raceTypes))
	return raceTypes[i]
}

func (r *RandomGenerator) generateMeetingLink(raceID uint) string {
	i := rand.Int31() + 1
	return fmt.Sprintf("%s/races/%d/%d", r.apiHost, raceID, i)
}

func (r *RandomGenerator) generateRace() *Race {
	raceType := r.randomRaceType()
	return &Race{
		Type:        raceType,
		ClosingTime: r.randomTime(),
		MeetingLink: r.generateMeetingLink(raceType.ID),
	}
}

// NextRaces retrieve the next N races (pages * pageSize)
func (r *RandomGenerator) NextRaces(pages uint) ([]Race, error) {
	nbNext := pageSize * pages
	races := make([]Race, nbNext)

	for i := uint(0); i < nbNext; i++ {
		races[i] = *r.generateRace()
	}

	// With a "normal" database, we will do this sorting at query level.
	comparisonClosure := func(i int, j int) bool {
		return races[i].ClosingTime.Before(races[j].ClosingTime)
	}
	sort.Slice(races, comparisonClosure)
	return races, nil
}

// Close to close the pending resources
func (r *RandomGenerator) Close() {}
