/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-06 09:12:08
 * Last Modified: 2017-12-06 21:12:27
 * Modified By: Gaston Siffert
 */

package main

import (
	"math/rand"
	"time"

	"bitbucket.org/gaston_siffert/ladbrokes/api/database"
	"bitbucket.org/gaston_siffert/ladbrokes/api/server"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func supportCORS() gin.HandlerFunc {
	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	return cors.New(config)
}

func main() {
	rand.Seed(time.Now().UTC().UnixNano())

	db := database.NewRandomGenerator()
	defer db.Close()

	// Instantiate the Server
	s := server.NewDefaultServer(db)
	// Configure the routes to serve
	e := gin.Default()
	e.Use(supportCORS())
	server.SetRoutes(s, e, routes)
	e.Run()
}
