/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-06 09:12:28
 * Last Modified: 2017-12-06 11:12:13
 * Modified By: Gaston Siffert
 */

package server

import "bitbucket.org/gaston_siffert/ladbrokes/api/database"

// DefaultServer implement the Server interface
type DefaultServer struct {
	db database.Database
}

// GetDatabase retrieve our Database wrapper
func (s *DefaultServer) GetDatabase() database.Database {
	return s.db
}

// NewDefaultServer will instantiate and initalize a new server
func NewDefaultServer(db database.Database) *DefaultServer {
	s := DefaultServer{
		db: db,
	}
	return &s
}
