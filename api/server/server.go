/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-06 10:12:13
 * Last Modified: 2017-12-06 11:12:16
 * Modified By: Gaston Siffert
 */

package server

import (
	"bitbucket.org/gaston_siffert/ladbrokes/api/database"
	"github.com/gin-gonic/gin"
)

// Server interface used to aggregate the required resources for our controllers
type Server interface {
	// GetDatabase retrieve our Database wrapper
	GetDatabase() database.Database
}

// SetRoutes configure the routes for the API and inject our Server
// with the Gin context
func SetRoutes(s Server, e *gin.Engine, routes []Route) {
	for _, route := range routes {
		e.Handle(
			route.Method, route.Path,
			inject(route.Handler, s),
		)
	}
}

// Handler used to inject the server into the controllers through a closure
type Handler func(s Server, c *gin.Context)

// Inject will override the default gin Handler with a closure giving access
// to our server
func inject(handler Handler, s Server) gin.HandlerFunc {
	return func(c *gin.Context) {
		handler(s, c)
	}
}

// Route describe a REST API endpoint
type Route struct {
	Method  string
	Path    string
	Handler Handler
}
