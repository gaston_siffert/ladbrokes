/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-06 09:12:31
 * Last Modified: 2017-12-06 11:12:32
 * Modified By: Gaston Siffert
 */

package race

import (
	"errors"
	"net/http"
	"strconv"

	"bitbucket.org/gaston_siffert/ladbrokes/api/database"
	"bitbucket.org/gaston_siffert/ladbrokes/api/server"
	"github.com/gin-gonic/gin"
)

const (
	errInvalidGetRaceRequest = "invalid get race request"
)

type getRaceRequest struct {
	RateTypeID int
	ID         int
}

// Sadly Gin doesn't support any Binding for the path parameters
func (g *getRaceRequest) load(c *gin.Context) error {
	raceTypeID := c.Param("race_type_id")
	id := c.Param("id")

	i, err := strconv.Atoi(raceTypeID)
	if err != nil {
		return err
	}
	g.RateTypeID = i

	i, err = strconv.Atoi(id)
	if err != nil {
		return err
	}
	g.ID = i

	return nil
}

// Since we don't have the Gin binding, we can't use the validator and I am
// a bit lazy to instantiate my own just for this request.
// https://godoc.org/gopkg.in/go-playground/validator.v9
func (g *getRaceRequest) isValid() bool {
	return g.ID > 0 && g.RateTypeID > 0
}

// GetRace lookup for a scheduled race and return it in JSON
func GetRace(s server.Server, c *gin.Context) {
	request := getRaceRequest{}
	if err := request.load(c); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	if !request.isValid() {
		err := errors.New(errInvalidGetRaceRequest)
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	race, err := s.GetDatabase().
		SearchRace(uint(request.RateTypeID), uint(request.ID))
	if err != nil {
		statusCode := http.StatusInternalServerError
		if err.Error() == database.ErrInvalidRaceTypeID {
			statusCode = http.StatusBadRequest
		}
		c.AbortWithError(statusCode, err)
		return
	}

	c.JSON(http.StatusOK, race)
}
