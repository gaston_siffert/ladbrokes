/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-06 09:12:05
 * Last Modified: 2017-12-06 11:12:35
 * Modified By: Gaston Siffert
 */

package database

// Competitor represent a racer entity
type Competitor struct {
	// ID primary key and unique identifier
	ID uint `json:"id"`
	// Name of the racer
	Name string `json:"name"`
}

// Meeting represent a meeting place.
// There aren't any requirements for a meeting page... How should I show it ?
type Meeting struct {
	// ID primary key and unique identifier
	ID uint `json:"id"`
	// City in which the meeting is scheduled
	City string `json:"city"`
	// Races scheduled for this meeting place
	Races []Race `json:"races"`
}

// Race represent a scheduled race
type Race struct {
	ID          uint         `json:"id"`
	Type        RaceType     `json:"race_type"`
	Competitors []Competitor `json:"competitors"`
}

// RaceType represent an allowed type of breeding race
type RaceType struct {
	// ID primary key and unique identifier
	ID uint `json:"id"`
	// Name of the breeding race
	Name string `json:"name"`
}
