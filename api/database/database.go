/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-06 09:12:13
 * Last Modified: 2017-12-06 11:12:42
 * Modified By: Gaston Siffert
 */

package database

// Database interface to wrap the query in our database
type Database interface {
	// SearchRaceType retrieve a RaceType via its ID
	SearchRaceType(id uint) (*RaceType, error)
	// SearchRace retrieve a Race via its ID and RaceTypeID.
	//
	// The raceTypeID shouldn't be there, I only used it to stay coherent
	// between the next5 project and this API.
	// Since we don't really use a database, I had to share this data through the
	// request.
	SearchRace(raceTypeID uint, id uint) (*Race, error)
	// Close to close the pending resources
	Close()
}
