/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-06 09:12:25
 * Last Modified: 2017-12-06 11:12:41
 * Modified By: Gaston Siffert
 */

package database

import (
	"errors"
	"math/rand"

	randomdata "github.com/Pallinder/go-randomdata"
)

const (
	maxCompetitor = 15
	minCompetitor = 4
	// ErrInvalidRaceTypeID error when we don't find a race type
	ErrInvalidRaceTypeID = "invalid race type id"
)

// of course this static dataset would be replaced by a table in a database
var raceTypes = map[uint]RaceType{
	1: RaceType{ID: 1, Name: "thoroughbred"},
	2: RaceType{ID: 2, Name: "greyhounds"},
	3: RaceType{ID: 3, Name: "harness"},
}

// RandomGenerator implement the Database interface with some random values
type RandomGenerator struct{}

// NewRandomGenerator instantiate a new RandomGenerator
func NewRandomGenerator() *RandomGenerator {
	return &RandomGenerator{}
}

// SearchRaceType retrieve a RaceType via its ID
func (r *RandomGenerator) SearchRaceType(id uint) (*RaceType, error) {
	if id > uint(len(raceTypes)) {
		return nil, errors.New(ErrInvalidRaceTypeID)
	}
	raceType := raceTypes[id]
	return &raceType, nil
}

func (r *RandomGenerator) generateCompetitor() *Competitor {
	id := rand.Int()
	name := randomdata.SillyName()
	return &Competitor{
		ID:   uint(id),
		Name: name,
	}
}

func (r *RandomGenerator) generateMeeting() *Race {
	nbCompetitors := rand.Intn(maxCompetitor) + minCompetitor
	competitors := make([]Competitor, nbCompetitors)

	for i := 0; i < nbCompetitors; i++ {
		competitors[i] = *r.generateCompetitor()
	}
	return &Race{
		Competitors: competitors,
	}
}

// SearchRace retrieve a Race via its ID and RaceTypeID.
func (r *RandomGenerator) SearchRace(raceTypeID uint, id uint) (*Race, error) {
	raceType, err := r.SearchRaceType(raceTypeID)
	if err != nil {
		return nil, err
	}
	race := r.generateMeeting()
	race.ID = id
	race.Type = *raceType
	return race, nil
}

// Close to close the pending resources
func (r *RandomGenerator) Close() {}
