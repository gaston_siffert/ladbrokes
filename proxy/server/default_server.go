/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-06 11:12:07
 * Last Modified: 2017-12-08 10:12:11
 * Modified By: Gaston Siffert
 */

package server

import (
	"context"
	"net/http"
	"sync"

	"bitbucket.org/gaston_siffert/ladbrokes/proxy/subscriber"
	"github.com/gorilla/websocket"
)

type cache struct {
	sync.Mutex
	bytes []byte
}

// DefaultServer implement the Server interface
type DefaultServer struct {
	upgrader    *websocket.Upgrader
	builder     subscriber.Builder
	lastRequest cache
}

func (s *DefaultServer) Upgrade(w http.ResponseWriter, r *http.Request) (*websocket.Conn, error) {
	return s.upgrader.Upgrade(w, r, nil)
}

func (s *DefaultServer) NewSubscriber() (subscriber.Subscriber, error) {
	return s.builder()
}

func (s *DefaultServer) subscribe(ctx context.Context) error {
	subscriber, err := s.NewSubscriber()
	if err != nil {
		return err
	}
	defer subscriber.Close()

	for {
		select {
		case <-ctx.Done():
			return nil
		default:
			bytes, err := subscriber.Read()
			if err != nil {
				return err
			}
			s.lastRequest.bytes = bytes
		}
	}
}

func (s *DefaultServer) EnableCache() {
	go s.subscribe(context.Background())
}

func (s *DefaultServer) GetLastRequest() []byte {
	return s.lastRequest.bytes
}

// NewDefaultServer will instantiate and initalize a new server
func NewDefaultServer(upgrader *websocket.Upgrader, builder subscriber.Builder) *DefaultServer {
	s := DefaultServer{
		upgrader: upgrader,
		builder:  builder,
	}
	return &s
}
