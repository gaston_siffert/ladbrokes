/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-08 09:12:26
 * Last Modified: 2017-12-08 12:12:28
 * Modified By: Gaston Siffert
 */

package server

import (
	"context"
	"errors"
	"fmt"
	"testing"
	"time"

	"bitbucket.org/gaston_siffert/ladbrokes/proxy/subscriber"
	"github.com/stretchr/testify/assert"
)

const (
	alwaysFail = "always fail"
	data       = "Hello World !"
)

// func TestUpgrade(t *testing.T) {
// 	req := httptest.NewRequest("GET", "ws://example.com", nil)
// 	req.Header.Add("Connection", "upgrade")
// 	req.Header.Add("Upgrade", "websocket")
// 	req.Header.Add("Sec-Websocket-Version", "13")
// 	w := httptest.NewRecorder()
// 	s := DefaultServer{upgrader: &websocket.Upgrader{}}

// 	t.Run(fmt.Sprintf("%v", s), func(t *testing.T) {
// 		ws, err := s.Upgrade(w, req)
// 		if ws != nil {
// 			ws.Close()
// 		}
// 		assert.Nil(t, err)
// 		assert.NotNil(t, ws)
// 	})
// }

type newSubscriberTest struct {
	builder  subscriber.Builder
	err      error
	expected subscriber.Subscriber
}

func alwaysFailBuilder() (subscriber.Subscriber, error) {
	return nil, errors.New(alwaysFail)
}

func alwaysSucceedBuilder() (subscriber.Subscriber, error) {
	return &subscriber.ZMQSubscriber{}, nil
}

var newSubscriberTests = []newSubscriberTest{
	newSubscriberTest{
		builder: alwaysFailBuilder,
		err:     errors.New(alwaysFail),
	},
	newSubscriberTest{
		builder:  alwaysSucceedBuilder,
		expected: &subscriber.ZMQSubscriber{},
	},
}

func testNewSubscriber(t *testing.T, test newSubscriberTest) {
	s := DefaultServer{builder: test.builder}

	t.Run(fmt.Sprintf("%v", test), func(t *testing.T) {
		subscriber, err := s.NewSubscriber()
		assert.Equal(t, test.err, err)
		assert.Equal(t, test.expected, subscriber)
	})
}

func TestNewSubscriber(t *testing.T) {
	for _, test := range newSubscriberTests {
		testNewSubscriber(t, test)
	}
}

type subscribeTest struct {
	builder  subscriber.Builder
	err      error
	expected []byte
}

type helloSubscriber struct{}

func (h helloSubscriber) Read() ([]byte, error) {
	return []byte(data), nil
}
func (h helloSubscriber) Close() {}

type alwaysFailSubscriber struct{}

func (a alwaysFailSubscriber) Read() ([]byte, error) {
	return nil, errors.New(alwaysFail)
}
func (a alwaysFailSubscriber) Close() {}

func mockedSubscriberBuilder(sub subscriber.Subscriber) subscriber.Builder {
	return func() (subscriber.Subscriber, error) {
		return sub, nil
	}
}

var subscribeTests = []subscribeTest{
	subscribeTest{
		builder: alwaysFailBuilder,
		err:     errors.New(alwaysFail),
	},
	subscribeTest{
		builder: mockedSubscriberBuilder(alwaysFailSubscriber{}),
		err:     errors.New(alwaysFail),
	},
	subscribeTest{
		builder:  mockedSubscriberBuilder(helloSubscriber{}),
		expected: []byte(data),
	},
}

func testSubscribe(t *testing.T, test subscribeTest) {
	s := DefaultServer{builder: test.builder}

	t.Run(fmt.Sprintf("%v", test), func(t *testing.T) {
		if test.err != nil {
			err := s.subscribe(context.Background())
			assert.Equal(t, test.err, err)
		} else {
			ctx, cancel := context.WithCancel(context.Background())
			go s.subscribe(ctx)
			time.Sleep(1 * time.Second)
			cancel()
			assert.Equal(t, test.expected, s.lastRequest.bytes)
		}
	})
}

func TestSubscribe(t *testing.T) {
	for _, test := range subscribeTests {
		testSubscribe(t, test)
	}
}

type getLastRequestTest struct {
	data []byte
}

var getLastRequestTests = []getLastRequestTest{
	getLastRequestTest{
		data: []byte{},
	},
	getLastRequestTest{
		data: []byte("simple test"),
	},
	getLastRequestTest{
		data: []byte("last one"),
	},
}

func testGetLastRequest(t *testing.T, test getLastRequestTest) {
	s := DefaultServer{}
	s.lastRequest.bytes = test.data

	t.Run(fmt.Sprintf("%v", test), func(t *testing.T) {
		bytes := s.GetLastRequest()
		assert.Equal(t, test.data, bytes)
	})
}

func TestGetLastRequest(t *testing.T) {
	for _, test := range getLastRequestTests {
		testGetLastRequest(t, test)
	}
}
