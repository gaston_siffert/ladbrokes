/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-06 11:12:18
 * Last Modified: 2017-12-06 22:12:32
 * Modified By: Gaston Siffert
 */

package server

import (
	"net/http"

	"bitbucket.org/gaston_siffert/ladbrokes/proxy/subscriber"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

// Server interface used to aggregate the required resources for our controllers
type Server interface {
	// Upgrade a HTTP request to a WebSocket connection
	Upgrade(w http.ResponseWriter, r *http.Request) (*websocket.Conn, error)
	// NewSubscriber build a new Subscriber
	NewSubscriber() (subscriber.Subscriber, error)
	// EnableCache enable a cache for the last request, to send for the new
	// clients. Otherwise they would have to wait the next event.
	EnableCache()
	// GetLastRequest return the bytes from the last request
	GetLastRequest() []byte
}

// SetRoutes configure the routes for the API and inject our Server
// with the Gin context
func SetRoutes(s Server, e *gin.Engine, routes []Route) {
	for _, route := range routes {
		e.Handle(
			route.Method, route.Path,
			inject(route.Handler, s),
		)
	}
}

// Handler used to inject the server into the controllers through a closure
type Handler func(s Server, c *gin.Context)

// Inject will override the default gin Handler with a closure giving access
// to our server
func inject(handler Handler, s Server) gin.HandlerFunc {
	return func(c *gin.Context) {
		handler(s, c)
	}
}

// Route describe a REST API endpoint
type Route struct {
	Method  string
	Path    string
	Handler Handler
}
