/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-08 10:12:48
 * Last Modified: 2017-12-08 13:12:28
 * Modified By: Gaston Siffert
 */

package controllers

import (
	"errors"
	"fmt"
	"testing"

	"bitbucket.org/gaston_siffert/ladbrokes/proxy/subscriber"
)

const (
	alwaysFail = "always fail"
	data       = "Hello World"
)

func alwaysFailBuilder() (subscriber.Subscriber, error) {
	return nil, errors.New(alwaysFail)
}

type helloSubscriber struct{}

func (h helloSubscriber) Read() ([]byte, error) {
	return []byte(data), nil
}
func (h helloSubscriber) Close() {}

type alwaysFailSubscriber struct{}

func (a alwaysFailSubscriber) Read() ([]byte, error) {
	return nil, errors.New(alwaysFail)
}
func (a alwaysFailSubscriber) Close() {}

func mockedSubscriberBuilder(sub subscriber.Subscriber) subscriber.Builder {
	return func() (subscriber.Subscriber, error) {
		return sub, nil
	}
}

type streamTest struct {
	builder  subscriber.Builder
	err      error
	expected []byte
}

var streamTests = []streamTest{
	streamTest{
		builder: alwaysFailBuilder,
		err:     errors.New(""),
	},
	streamTest{
		builder: mockedSubscriberBuilder(alwaysFailSubscriber{}),
		err:     errors.New(""),
	},
	streamTest{
		builder:  mockedSubscriberBuilder(helloSubscriber{}),
		expected: []byte(data),
	},
}

// I am wasting a bit too much time on this so I will comment it.
// I haven't found a way to open a websocket with Gin: r.ServeHTTP
func testStream(t *testing.T, test streamTest) {
	// gin.SetMode(gin.TestMode)
	// r := gin.Default()
	// s := server.NewDefaultServer(&websocket.Upgrader{}, test.builder)
	// sub, err := s.NewSubscriber()
	// r.GET("/stream", func(c *gin.Context) { Stream(s, c) })
	// req, _ := http.NewRequest(http.MethodGet, "/stream", nil)
	// // websocket.NewClient()
	// // websocket.
	// // fmt.Println(req.RequestURI)
	// // websocket.DefaultDialer.Dial(u.String(), nil)
	// w := httptest.NewRecorder()
	// // r.ser
	// // r.HandleContext(func(c *gin.Context) { Stream(s, c) })

	// Stream(s, gin.Context{Writer: w, Request: r})

	t.Run(fmt.Sprintf("%v", test), func(t *testing.T) {
		// conn, _, err := websocket.DefaultDialer.Dial("ws://localhost:8080/stream", nil)
		// Invalid test
		// if err != nil {
		// 	log.Fatal(err)
		// }
		// defer conn.Close()

		// time.Sleep(1 * time.Second)
		// _, bytes, err := conn.ReadMessage()
		// log.Println("error", err)
		// if test.err != nil {
		// 	assert.NotNil(t, err)
		// } else {
		// 	assert.Nil(t, err)
		// 	assert.Equal(t, test.expected, bytes)
		// }
	})
}

func TestStream(t *testing.T) {
	for _, test := range streamTests {
		testStream(t, test)
	}
}
