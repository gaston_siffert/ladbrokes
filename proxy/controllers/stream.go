/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-06 12:12:53
 * Last Modified: 2017-12-08 14:12:27
 * Modified By: Gaston Siffert
 */

package controllers

import (
	"net/http"

	"bitbucket.org/gaston_siffert/ladbrokes/proxy/server"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

// Stream will proxy the incoming publisher's event to a websocket
func Stream(s server.Server, c *gin.Context) {
	// Open a websocket connection with the client
	conn, err := s.Upgrade(c.Writer, c.Request)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	defer conn.Close()

	subscriber, err := s.NewSubscriber()
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	defer subscriber.Close()

	// Send the last request to avoid the client to wait
	conn.WriteMessage(websocket.TextMessage, s.GetLastRequest())
	for {
		bytes, err := subscriber.Read()
		if err != nil {
			c.AbortWithError(http.StatusInternalServerError, err)
			return
		}

		if err := conn.WriteMessage(websocket.TextMessage, bytes); err != nil {
			c.AbortWithError(http.StatusInternalServerError, err)
			return
		}
	}
}
