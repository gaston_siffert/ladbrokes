/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-07 19:12:47
 * Last Modified: 2017-12-07 19:12:10
 * Modified By: Gaston Siffert
 */

package main

import (
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

type getConfigFromFileTest struct {
	fileName string
	env      string
	config   *configuration
	err      error
}

var getConfigFromFileTests = []getConfigFromFileTest{
	getConfigFromFileTest{
		fileName: "not_found.yaml",
		err:      errors.New("open not_found.yaml: no such file or directory"),
	},
	getConfigFromFileTest{
		fileName: "",
		err:      errors.New("open : no such file or directory"),
	},
	getConfigFromFileTest{
		fileName: "main.go",
		err:      errors.New("yaml: line 1: mapping values are not allowed in this context"),
	},
	getConfigFromFileTest{
		fileName: "config.yaml",
		env:      "production",
		err:      errors.New("the env: production hasn't been found in the file ./config.yaml"),
	},
	getConfigFromFileTest{
		fileName: "config.yaml",
		env:      "development",
		config: &configuration{
			PublisherHost: "next5:8080",
			Topic:         "next5",
		},
	},
}

func testGetConfigFromFile(t *testing.T, test getConfigFromFileTest) {
	t.Run(fmt.Sprintf("%v", test), func(t *testing.T) {
		config, err := getConfigFromFile(test.fileName, test.env)
		assert.Equal(t, test.config, config)
		if test.err != nil {
			assert.Equal(t, test.err.Error(), err.Error())
		}
	})
}

func TestGetConfigFromFile(t *testing.T) {
	for _, test := range getConfigFromFileTests {
		testGetConfigFromFile(t, test)
	}
}
