/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-06 11:12:48
 * Last Modified: 2017-12-06 21:12:38
 * Modified By: Gaston Siffert
 */

package main

import (
	"log"
	"net/http"

	"bitbucket.org/gaston_siffert/ladbrokes/proxy/server"
	"bitbucket.org/gaston_siffert/ladbrokes/proxy/subscriber"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	zmq "github.com/pebbe/zmq4"
)

const (
	// There are a small bug with the inproc:// in zmq Golang libraries:
	// https://github.com/pebbe/zmq4/issues/120
	// If we want to dramatically scale up the proxy, we should implement it in
	// CPP.
	ipcFile = "to-clients.ipc"
)

func onError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	conf, err := getConfiguration()
	onError(err)

	ctx, err := zmq.NewContext()
	onError(err)
	defer ctx.Term()

	// Run a proxy server between the publisher and the websocket clients
	proxy, err := subscriber.NewProxy(ctx, conf.PublisherHost, ipcFile)
	onError(err)
	defer proxy.Close()
	go proxy.Proxy()

	builder := func() (subscriber.Subscriber, error) {
		return subscriber.NewZMQSubscriber(ctx, ipcFile, conf.Topic)
	}

	upgrader := &websocket.Upgrader{
		// We override the checkOrigin for the test, it shouldn't be done
		// in production
		CheckOrigin: func(r *http.Request) bool { return true },
	}
	s := server.NewDefaultServer(upgrader, builder)
	s.EnableCache()

	// Configure the routes to serve
	e := gin.Default()
	server.SetRoutes(s, e, routes)
	e.Run()
}
