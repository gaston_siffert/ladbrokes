/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-06 12:12:33
 * Last Modified: 2017-12-06 12:12:52
 * Modified By: Gaston Siffert
 */

package main

import (
	"net/http"

	"bitbucket.org/gaston_siffert/ladbrokes/proxy/controllers"
	"bitbucket.org/gaston_siffert/ladbrokes/proxy/server"
)

var routes = []server.Route{
	// The race_type_id shouldn't be there, I only used it to stay coherent
	// between the next5 project and this API.
	// Since we don't really use a database, I had to share this data through the
	// request.
	server.Route{
		Method:  http.MethodGet,
		Path:    "/",
		Handler: controllers.Stream,
	},
}
