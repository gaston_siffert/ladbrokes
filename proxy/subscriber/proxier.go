/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-08 10:12:32
 * Last Modified: 2017-12-08 10:12:48
 * Modified By: Gaston Siffert
 */

package subscriber

// Proxier interface used to proxy 2 sockets
type Proxier interface {
	// Proxy run a server which proxy the incoming message from a socket to another
	Proxy() error
	// Close to close the pending resources
	Close()
}
