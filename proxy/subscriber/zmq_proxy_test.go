/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-07 20:12:59
 * Last Modified: 2017-12-08 09:12:50
 * Modified By: Gaston Siffert
 */

package subscriber

import (
	"errors"
	"fmt"
	"log"
	"testing"

	zmq "github.com/pebbe/zmq4"
	"github.com/stretchr/testify/assert"
)

type newProxyTest struct {
	connectOn string
	listenOn  string
	err       error
}

var newProxyTests = []newProxyTest{
	newProxyTest{
		err: errors.New("invalid argument"),
	},
	newProxyTest{
		connectOn: "",
		listenOn:  "test.ipc",
		err:       errors.New("invalid argument"),
	},
	newProxyTest{
		connectOn: "host.com:8080",
		listenOn:  "",
		err:       errors.New("invalid argument"),
	},
	newProxyTest{
		connectOn: "not a protocol",
		listenOn:  "test.ipc",
		err:       errors.New("invalid argument"),
	},
	newProxyTest{
		connectOn: "host.com:8080",
		listenOn:  "test.ipc",
	},
}

func testNewZMQProxy(t *testing.T, test newProxyTest) {
	t.Run(fmt.Sprintf("%v", test), func(t *testing.T) {
		ctx, err := zmq.NewContext()
		// Invalid test
		if err != nil {
			log.Fatal(err)
		}
		_, err = NewProxy(ctx, test.connectOn, test.listenOn)
		if test.err != nil {
			assert.Equal(t, test.err.Error(), err.Error())
		}
	})
}

func TestNewZMQProxy(t *testing.T) {
	for _, test := range newProxyTests {
		testNewZMQProxy(t, test)
	}
}
