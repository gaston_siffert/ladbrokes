/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-06 12:12:36
 * Last Modified: 2017-12-06 13:12:06
 * Modified By: Gaston Siffert
 */

package subscriber

import (
	"errors"
	"fmt"

	zmq "github.com/pebbe/zmq4"
)

const (
	// ErrInvalidMessage is used when we receive a message from the publisher
	// without any Topic
	ErrInvalidMessage = "received an invalid message"
)

// ZMQSubscriber implement the Subscriber interface with ZMQ
type ZMQSubscriber struct {
	fromPublisher *zmq.Socket
}

// NewZMQSubscriber instantiate a new ZMQPublisher with connectOn as
// inproc connection and subscribe to the given topic
func NewZMQSubscriber(ctx *zmq.Context, connectOn string, topic string) (*ZMQSubscriber, error) {
	fromPublisher, err := ctx.NewSocket(zmq.SUB)
	if err != nil {
		return nil, err
	}

	err = fromPublisher.Connect(fmt.Sprintf("inproc://%s", connectOn))
	fromPublisher.SetSubscribe(topic)
	return &ZMQSubscriber{
		fromPublisher: fromPublisher,
	}, err
}

// Read will read a message from the socket
func (z *ZMQSubscriber) Read() ([]byte, error) {
	message, err := z.fromPublisher.RecvMessageBytes(0)
	if err != nil {
		return nil, err
	}
	if len(message) != 2 {
		return nil, errors.New(ErrInvalidMessage)
	}
	return message[1], nil
}

// Close to close the pending resources
func (z *ZMQSubscriber) Close() {
	z.fromPublisher.Close()
}
