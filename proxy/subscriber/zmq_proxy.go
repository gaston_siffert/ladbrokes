/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-06 12:12:52
 * Last Modified: 2017-12-06 14:12:28
 * Modified By: Gaston Siffert
 */

package subscriber

import (
	"fmt"

	zmq "github.com/pebbe/zmq4"
)

// ZMQProxy implement the Proxier interface with ZMQ
type ZMQProxy struct {
	fromPublisher *zmq.Socket
	toClients     *zmq.Socket
}

// NewProxy instantiate a new proxy, with connectOn as TCP connection
// and listenOn as inproc connection
func NewProxy(ctx *zmq.Context, connectOn string, listenOn string) (*ZMQProxy, error) {
	// Connect to the publisher stream
	fromPublisher, err := ctx.NewSocket(zmq.XSUB)
	if err != nil {
		return nil, err
	}
	if err := fromPublisher.Connect(fmt.Sprintf("tcp://%s", connectOn)); err != nil {
		return nil, err
	}

	// Proxy the stream to the websocket clients
	toClients, err := ctx.NewSocket(zmq.XPUB)
	if err != nil {
		return nil, err
	}
	err = toClients.Bind(fmt.Sprintf("inproc://%s", listenOn))

	return &ZMQProxy{
		fromPublisher: fromPublisher,
		toClients:     toClients,
	}, err
}

// Proxy will transfer the message from a publisher to the Goroutine running
// a websocket
func (z *ZMQProxy) Proxy() error {
	return zmq.Proxy(z.fromPublisher, z.toClients, nil)
}

// Close to close the pending resources
func (z *ZMQProxy) Close() {
	z.fromPublisher.Close()
	z.toClients.Close()
}
