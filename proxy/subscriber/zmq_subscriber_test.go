/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-08 09:12:59
 * Last Modified: 2017-12-08 09:12:41
 * Modified By: Gaston Siffert
 */

package subscriber

import (
	"errors"
	"fmt"
	"log"
	"testing"

	zmq "github.com/pebbe/zmq4"
	"github.com/stretchr/testify/assert"
)

type newZMQSubscriberTest struct {
	connectOn string
	topic     string
	err       error
}

// Should we forbid an empty string topic ?
var newZMQSubscriberTests = []newZMQSubscriberTest{
	newZMQSubscriberTest{
		err: errors.New("invalid argument"),
	},
	newZMQSubscriberTest{
		topic: "news",
		err:   errors.New("invalid argument"),
	},
	newZMQSubscriberTest{
		topic:     "news",
		connectOn: "test.ipc",
	},
}

func testNewZMQSubscriber(t *testing.T, test newZMQSubscriberTest) {
	t.Run(fmt.Sprintf("%v", test), func(t *testing.T) {
		ctx, err := zmq.NewContext()
		// Invalid test
		if err != nil {
			log.Fatal(err)
		}
		_, err = NewZMQSubscriber(ctx, test.connectOn, test.topic)
		if test.err != nil {
			assert.Equal(t, test.err.Error(), err.Error())
		}
	})
}

func TestNewZMQSubscriber(t *testing.T) {
	for _, test := range newZMQSubscriberTests {
		testNewZMQSubscriber(t, test)
	}
}
