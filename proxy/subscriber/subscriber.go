/**
 * Author: Gaston Siffert
 * Created Date: 2017-12-06 12:12:23
 * Last Modified: 2017-12-08 10:12:46
 * Modified By: Gaston Siffert
 */

package subscriber

// Subscriber interface used to subscribe from an event stream
type Subscriber interface {
	// Read from the event stream
	Read() ([]byte, error)
	// Close to close the pending resources
	Close()
}

// Builder closure used to instantiate a Subscriber
type Builder func() (Subscriber, error)
