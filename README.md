# ladbrokes

## Description

The projects is built on several parts:
- frontend
- api
- next5
- proxy

![Components](doc/components.png)

next5 is a "streamer" app, its only purpose is to read the data from any storage system.
At the moment, it self generate the information, I thought it would have been more relavant to have a
time relative to "now" and not a static set of data.

proxy is, you will guess, a proxy ! At the moment, ZeroMQ doesn't support "officialy" the websocket protocol.
To be able to stream our data toward the frontend app, I needed to proxy the ZMQ socket
to websocket. After several thought, this app could have been a part of the api/ but
I believe the websockets resources will be used more intensively than api/, hence, scaling both would have been a waste of resources. Including a redirect from the API to the proxy is a possiblity if you want to keep only one "domain" for the clients.

api, it's a simple REST API, it was supposed to serve the data for both the Race page and the Meeting page.
But I haven't really found any usage of the Meeting page, so at the moment it will only
support the Race page.

frontend, it's a ReactJS application. It might not be good looking and I might not respect
the design or recommendation for this technology. I just started to learn this technology some
weeks ago. But it respect the prerequist with an Index page showing the streamed data from the proxy
and you can click on any race to see its detailed page.

Nota bene: the race page is generated at every call, hence you won't see the same data
by refreshing the page.


## How to build

To build the project, you will need docker and docker-compose.
Moreover, the libraries in Golang are managed by Glide and there aren't
included in the repository. At the moment, the Dockerfile isn't downloading the libraries
at the moment, so you will have to go in each directory and do a:
> $> glide install

Example:
> $> cd api/ && glide install
>
> $> cd next5/ && glide install
>
> $> cd proxy/ && glide install

At the root of the repository, you have a docker-compose.yml helping you to run all the projects.
If you just want to run all the projects simultaneously, you can do a:
> $> docker-compose up all

To be sure that the project is properly running, you can do a:
> $> docker ps

And you should be able to see 4 projects:
- ladbrokes_next5
- ladbrokes_api
- ladbrokes_proxy
- ladbrokes_all or ladbrokes_frontend (if you ran them one by one)

Don't be fooled by the log of the frontend app, the application is accessible at:
> http://localhost:8080/

## Tests

To run the tests, I recommend to use the advised $(glide novendor).

> $> cd next5/ && go test $(glide novendor)
>
> $> cd proxy/ && go test $(glide novendor)

## Missing part

I was running a bit out of time, this sample took me longer than expected
and I believe the current state of the software is already good enough
to be submitted.

Below, you can find a list of missing part / improvements to the current solution:
- Tests: api/ and frontend/
- Logger: I didn't provide any abstraction for a logger, hence there aren't any log in the apps
- Library: It should have some libraries for the common part in Golang (logger, config, ...) and the API client in Javascript.
- Meeting: It isn't really described how to access this page, is it supposed to be the index page ?
